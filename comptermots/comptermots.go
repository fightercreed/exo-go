package comptermots

import (
	"strings"
)

// Count - Fonction pour compter les mots dans une chaine
func Count(phrase string) int {
	return len(strings.Fields(phrase))
}
